/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

import models.Contacto;
import util.Archivo;
import views.RegistroContactos;
import views.TablaContactos;

/**
 *
 * @author Christian
 */
public class RegistroController2 implements ActionListener{
    private RegistroContactos rc;
    private TablaContactos t;
   

    public RegistroController2(RegistroContactos rc) {
        
        this.rc = rc;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "guardar":
                save();
            case "mostrar":
               showContacts();
                break;
            case "limpiar":
                clear();
                break;
            case "salir":
                salir();
                break;
            
        }
    }
    
    private void save(){
        Archivo archivo = new Archivo();
        archivo.crearArchivo();
        
        String nombre,apellido, telefono,direccion, correo;
        
        nombre = rc.cajaNombre.getText();
        apellido = rc.cajaApellido.getText();
        telefono = rc.cajaNumero.getText();
        direccion= rc.cajaDireccion.getText();
        correo = rc.cajaCorreo.getText();
        
        
        
        Contacto contacto = new Contacto(nombre, apellido, telefono,direccion,correo);
        archivo.escribirTexto(contacto);
    }
    
    private void showContacts(){
     
        TablaContactos table = new TablaContactos(rc,false);
        table.setVisible(true);
            
    }
    
    private void clear(){
        rc.cajaNombre.setText("");
        rc.cajaApellido.setText("");
        rc.cajaNumero.setText("");
        rc.cajaDireccion.setText("");
        rc.cajaCorreo.setText("");
        
    }
    
    private void salir(){
        System.exit(0);
        rc.dispose();
    }
    
   
    
    
}
