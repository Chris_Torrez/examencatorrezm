/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Christian
 */
public class TablaContactos extends javax.swing.JDialog {
    DefaultTableModel tableModel = new DefaultTableModel();

    public TablaContactos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        cargarModeloTabla();
        setTitle("Agenda");
        initComponents();
        setLocationRelativeTo(null);
        
        leerTexto();
    }

    private void cargarModeloTabla(){
        tableModel.addColumn("Nombre");
        tableModel.addColumn("Apellido");
        tableModel.addColumn("Telefono");
        tableModel.addColumn("Tipo");
        tableModel.addColumn("Direccion");
        tableModel.addColumn("Tipo");
        tableModel.addColumn("Correo");
        tableModel.addColumn("Tipo");
    }
    
    private void leerTexto(){
       String cadena, fila[];
        try {
             FileReader lector = new FileReader("agendaContactos.txt");
              BufferedReader lectura = new BufferedReader(lector);
           try {
               cadena = lectura.readLine();
               while(cadena != null){
                   fila = cadena.split("%");
                   tableModel.addRow(fila);
                   
                   cadena = lectura.readLine();
               }
           } catch (IOException ex) {
               Logger.getLogger(TablaContactos.class.getName()).log(Level.SEVERE, null, ex);
           }
              
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TablaContactos.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PanelAgenda = new javax.swing.JPanel();
        LabelContactos = new javax.swing.JLabel();
        ScrollTable = new javax.swing.JScrollPane();
        TableAgenda = new javax.swing.JTable();
        regresarButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        LabelContactos.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        LabelContactos.setText("Contactos");

        TableAgenda.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        TableAgenda.setModel(tableModel);
        ScrollTable.setViewportView(TableAgenda);

        regresarButton.setText("Regresar");
        regresarButton.setToolTipText("");
        regresarButton.setActionCommand("regresar");
        regresarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                regresarButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelAgendaLayout = new javax.swing.GroupLayout(PanelAgenda);
        PanelAgenda.setLayout(PanelAgendaLayout);
        PanelAgendaLayout.setHorizontalGroup(
            PanelAgendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelAgendaLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(LabelContactos)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(PanelAgendaLayout.createSequentialGroup()
                .addComponent(ScrollTable, javax.swing.GroupLayout.DEFAULT_SIZE, 722, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelAgendaLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(regresarButton)
                .addGap(39, 39, 39))
        );
        PanelAgendaLayout.setVerticalGroup(
            PanelAgendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelAgendaLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(LabelContactos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ScrollTable, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(regresarButton)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PanelAgenda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PanelAgenda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void regresarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_regresarButtonActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_regresarButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelContactos;
    private javax.swing.JPanel PanelAgenda;
    private javax.swing.JScrollPane ScrollTable;
    private javax.swing.JTable TableAgenda;
    public javax.swing.JButton regresarButton;
    // End of variables declaration//GEN-END:variables
}
