/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Contacto;

/**
 *
 * @author Christian
 */
public class Archivo {
    File archivo;
    public void crearArchivo(){
        archivo = new File("agendaContactos.txt");
        
        try {
            if(archivo.createNewFile()){
                //Archivo creado
            }
        } catch (IOException ex) {
            System.err.println("Error, "+ex);
        }
    }
    
    public void escribirTexto(Contacto contacto){
        try {
            FileWriter escribir = new FileWriter(archivo, true);
            escribir.write("\r"+contacto.getNombre()+"%"+contacto.getApellido()+"%"+contacto.getTelefono()+"%"+contacto.getDireccion()+"%"+contacto.getCorreo()
            );
            escribir.close();
        } catch (IOException ex) {
            System.err.println("Error, "+ex);
        }
        
    }
}
